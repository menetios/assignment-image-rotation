#pragma once

#include "bmp.h"

read_status read_bmp(FILE* in, image* img);

size_t get_bmp_padding(const image* img);

size_t get_bmp_pixels_size(const image *img);

size_t get_bmp_size(const image* img);

read_status read_bmp_header(FILE* in, image* img);

read_status read_bmp_pixels(FILE* in, image* img);
