#pragma once


#include "bmp.h"


write_status write_bmp(FILE* out, const image* img);

write_status write_bmp_header(FILE* out, const image* img);

write_status write_bmp_pixels(FILE* out, const image* img);



