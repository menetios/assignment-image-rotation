#pragma once

#include <stdio.h>
#include "image.h"


typedef enum {
    WRITE_OK = 0,
    WRITE_INVALID_FILE,
    WRITE_UNKNOWN_FORMAT,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_DATA
} write_status;



write_status write_image(const char* path, const image* img);

