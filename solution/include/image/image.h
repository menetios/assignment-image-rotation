#pragma once

#include <stdint.h>
#include <stdlib.h>


typedef struct {
    uint8_t b, g, r;
} pixel;

typedef struct {
  uint64_t width, height;
    pixel* data;
} image;

#include "read.h"
#include "write.h"

typedef struct {
    const char *extension;
    read_status (*read_image)(FILE* in, image* img);
    write_status (*write_image)(FILE* out, const image* img);
} image_format;


image_format* get_file_image_format(const char *path);
