#pragma once

#include <stdio.h>
#include "image.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef enum {
    READ_OK = 0,
    READ_INVALID_FILE,
    READ_UNKNOWN_FORMAT,
    READ_INVALID_HEADER,
    READ_INVALID_DATA,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS
} read_status;

read_status read_image(const char* path, image* img);



