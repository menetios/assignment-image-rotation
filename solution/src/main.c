#include <stdio.h>

#include "transformations/rotation.h"
#include "image/image.h"



int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Correct format : image-transform `dst` `src`");
        return -1;
    }
    image src;
    read_status status = read_image(argv[1], &src);
    if (status != READ_OK) {
        printf("ERROR: %d", status);
        return status;
    }
    image *dst = rotate(&src);
    return write_image(argv[2], dst);
}
