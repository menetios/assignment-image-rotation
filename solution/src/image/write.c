#include "image/image.h"
#include <string.h>
#include <ctype.h>


write_status write_image(const char* path, image const* img) {
    image_format* format = get_file_image_format(path);
    if (!format) return WRITE_UNKNOWN_FORMAT;
    FILE* file = fopen(path, "w");
    if (!file) return WRITE_INVALID_FILE;
    write_status status = format->write_image(file, img);
    fclose(file);
    return status;
}
