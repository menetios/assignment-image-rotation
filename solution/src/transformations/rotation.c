#include "transformations/rotation.h"
#include <stdio.h>

image *rotate(const image* source) {
    image *destination = (image*) malloc(sizeof(image));
    destination->width = source->height;
    destination->height = source->width;
    destination->data = (pixel*) malloc(sizeof(pixel) * destination->width * destination->height);
    for (size_t i = 0; i < destination->height; ++i) {
        for (size_t j = 0; j < destination->width; ++j) {
            destination->data[i * destination->width + j] = source->data[(source->height - j - 1) * source->width + i];
        }
    }
    return destination;
}
